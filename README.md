# About the project

**PoGo Types Quiz** is a quiz on Pokémon type strengths and weaknesses in Pokémon Go, an AR mobile game developed by Niantic, Nintendo and The Pokémon Company (I have no affiliation with either of them). This quiz is an example for [Android Kotlin Fundamentals: codelabs 03.1-03.3](https://codelabs.developers.google.com/codelabs/kotlin-android-training-create-and-add-fragment/index.html?index=..%2F..android-kotlin-fundamentals#0): creating fragments, adding navigation paths and starting an external activity. The code is based on AndroidTrivia quiz app, with a few changes. In particular, the player always has to answer 10 questions, and correct answers are displayed afterwards. Questions are generated randomly from the hardcoded Pokémon Go types chart found on the Internet.

Some images used in the app are from amazing free _Pokemon Go Icon Pack_ by [Roundicons Freebies](https://www.flaticon.com/authors/roundicons-freebies). The type images are somewhere from the Internet - the same images used by a lot of type charts, so they're hopefully free for fair use.

The apk is here: [pogotypes.apk](apk/pogotypes.apk).

Bonus: a [text-only version](console_version) I used to test generating random questions. It requires [command line Kotlin compiler](https://kotlinlang.org/docs/tutorials/command-line.html) to build it.
___

My blog: [Cat's Mysterious Box](http://catness.org/logbook/)



