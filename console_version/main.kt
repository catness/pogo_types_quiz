
fun process_input(input : String?) : Int {
    var ans: Int = try { 
        input!!.toInt()
    } catch (e: NumberFormatException) { 
        -1
    } 
    when (ans) {
        in 1..4 -> { 
            println("Your answer was $ans.")
            return ans
        }
        -1 -> {
            println("Invalid input $input: not a number. Please try again.")
            return -1
        }
        else -> {
            println("Invalid input $input: number is out of range. Please try again.")
            return -1
        }
    }
}

fun generateQuestion(counter : Int) : Triple<String,String,Int> {
    var size: Int = types.size-1
    var numType = (0..size).random()   // type we choose in the question
    var quality = (0..3).random()
    val debug : Boolean = false
    
    if (debug) println("\n\n\n${types[numType].type} ${qualities[quality]} :")
    val correctList : List<Type> = 
        when(quality) {
            0 -> types[numType].strong
            1 -> types[numType].weak
            2 -> types[numType].resist
            3 -> types[numType].vulnerable
            else -> listOf(Type.None) // shouldn't happen
        }
   
    var sizeCorrect : Int = correctList.size-1
    var correct = (0..sizeCorrect).random()   // one of the correct answers : number
    val correctAnswer = correctList[correct]  // the value of the correct answer

    if (debug) println("Correct: ${correctList} *$correctAnswer")
    val incorrectList : List<Type> = allTypes.minus(correctList)

    val shuffled = incorrectList.shuffled()
    var answers : List<Type> = listOf(shuffled[0],shuffled[1],shuffled[2],correctAnswer).shuffled()
    if (debug) println("Incorrect: ${incorrectList} * ${answers}")
    val question = """Question $counter.

${types[numType].type} ${qualities[quality]}: 
    1. ${answers[0]}
    2. ${answers[1]}
    3. ${answers[2]}
    4. ${answers[3]}
"""
    return Triple(question,correctAnswer.toString(),answers.indexOf(correctAnswer))
}


fun main() {

    var counter : Int = 1
    var answer : Int
    val max : Int = 10  
    var numCorrect : Int = 0

    repeat(max) {
        var (question, correctAnswer, correct) = generateQuestion(counter++)
        println("\n\n" + question)
        do {
            println("Please enter a number from 1 to 4:")
            var input = readLine()
            answer = process_input(input)
        } while (answer < 0)
        if (answer == correct+1) { // user's answers start from 1, real answers start from 0
            println("Correct!")
            numCorrect++
        }
        else {
            println("Incorrect! The answer is $correctAnswer.")
        }
    }
    println("\nCorrect answers: $numCorrect out of $max.")
}