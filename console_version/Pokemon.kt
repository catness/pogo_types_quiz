enum class Type {
    Normal,
    Fighting,
    Flying,
    Poison,
    Ground,
    Rock,
    Bug,
    Ghost,
    Steel,
    Fire,
    Water,
    Grass,
    Electric,
    Psychic,
    Ice,
    Dragon,
    Dark,
    Fairy,
    None
}

val qualities : List<String> = listOf("is strong against","is weak against","resists","is vulnerable to")
val allTypes : List<Type> = listOf(
    Type.Normal,
    Type.Fighting,
    Type.Flying,
    Type.Poison,
    Type.Ground,
    Type.Rock,
    Type.Bug,
    Type.Ghost,
    Type.Steel,
    Type.Fire,
    Type.Water,
    Type.Grass,
    Type.Electric,
    Type.Psychic,
    Type.Ice,
    Type.Dragon,
    Type.Dark,
    Type.Fairy)

data class TypeChart(val type : Type, val strong : List<Type>, val weak : List<Type>, 
        val resist : List<Type> , val vulnerable : List<Type>)

val types : List<TypeChart> = listOf(
    TypeChart(Type.Normal, 
        listOf(Type.None), 
        listOf(Type.Rock,Type.Steel,Type.Ghost), 
        listOf(Type.Ghost),
        listOf(Type.Fighting)),
    
    TypeChart(Type.Fighting, 
        listOf(Type.Normal, Type.Rock, Type.Steel, Type.Ice, Type.Dark),
        listOf(Type.Flying, Type.Poison, Type.Bug, Type.Psychic, Type.Ghost, Type.Fairy),
        listOf(Type.Rock, Type.Bug, Type.Dark),
        listOf(Type.Flying, Type.Psychic, Type.Fairy)),

    TypeChart(Type.Flying,
        listOf(Type.Fighting, Type.Bug, Type.Grass),
        listOf(Type.Rock, Type.Steel, Type.Electric),
        listOf(Type.Ground, Type.Fighting, Type.Bug, Type.Grass),
        listOf(Type.Rock, Type.Electric, Type.Ice)),

    TypeChart(Type.Poison,
        listOf(Type.Grass, Type.Fairy),
        listOf(Type.Poison, Type.Ground, Type.Rock, Type.Ghost, Type.Steel, Type.Bug),
        listOf(Type.Fighting, Type.Poison, Type.Grass, Type.Fairy),
        listOf(Type.Ground, Type.Psychic)),

    TypeChart(Type.Ground,
        listOf(Type.Poison, Type.Rock, Type.Steel, Type.Fire, Type.Electric),
        listOf(Type.Bug, Type.Grass, Type.Flying),
        listOf(Type.Poison, Type.Rock, Type.Electric),
        listOf(Type.Water, Type.Grass, Type.Ice)),

    TypeChart(Type.Rock,
        listOf(Type.Flying, Type.Bug, Type.Fire, Type.Ice),
        listOf(Type.Fighting, Type.Ground, Type.Steel),
        listOf(Type.Poison, Type.Flying, Type.Poison, Type.Fire),
        listOf(Type.Fighting, Type.Ground, Type.Steel, Type.Water, Type.Grass)),

    TypeChart(Type.Bug,
        listOf(Type.Grass, Type.Psychic, Type.Dark),
        listOf(Type.Fighting, Type.Flying, Type.Poison, Type.Ghost, Type.Steel, Type.Fire, Type.Fairy),
        listOf(Type.Fighting, Type.Ground, Type.Grass),
        listOf(Type.Flying, Type.Rock, Type.Fire)),

    TypeChart(Type.Ghost,
        listOf(Type.Ghost, Type.Psychic),
        listOf(Type.Dark, Type.Normal),
        listOf(Type.Normal, Type.Fighting, Type.Poison, Type.Bug),
        listOf(Type.Ghost, Type.Dark)),

    TypeChart(Type.Steel,
        listOf(Type.Rock, Type.Ice, Type.Fairy), 
        listOf(Type.Steel, Type.Fire, Type.Water, Type.Electric),
        listOf(Type.Normal, Type.Flying, Type.Poison, Type.Rock, Type.Bug, Type.Steel, Type.Grass, Type.Psychic, Type.Ice, Type.Dragon, Type.Fairy),
        listOf(Type.Fighting, Type.Fire, Type.Ground)),

    TypeChart(Type.Fire,
        listOf(Type.Bug, Type.Steel,Type.Grass, Type.Ice),
        listOf(Type.Rock, Type.Fire, Type.Water, Type.Dragon),
        listOf(Type.Bug, Type.Steel, Type.Fire, Type.Grass, Type.Ice, Type.Fairy),
        listOf(Type.Ground, Type.Rock, Type.Water)),

    TypeChart(Type.Water,
        listOf(Type.Ground, Type.Rock, Type.Fire),
        listOf(Type.Water, Type.Grass, Type.Dragon),
        listOf(Type.Steel, Type.Fire, Type.Water, Type.Ice),
        listOf(Type.Grass, Type.Electric)),

    TypeChart(Type.Grass,
        listOf(Type.Ground, Type.Rock, Type.Water),
        listOf(Type.Flying, Type.Poison, Type.Bug, Type.Steel, Type.Fire, Type.Grass, Type.Dragon),
        listOf(Type.Water, Type.Grass, Type.Electric, Type.Ground),
        listOf(Type.Flying, Type.Poison, Type.Bug, Type.Fire, Type.Ice)),

    TypeChart(Type.Electric,
        listOf(Type.Flying, Type.Water),
        listOf(Type.Ground, Type.Grass, Type.Electric, Type.Dragon),
        listOf(Type.Flying, Type.Steel, Type.Electric),
        listOf(Type.Ground)),

    TypeChart(Type.Psychic,
        listOf(Type.Fighting, Type.Poison),
        listOf(Type.Steel, Type.Psychic, Type.Dark),
        listOf(Type.Fighting, Type.Psychic),
        listOf(Type.Bug, Type.Ghost, Type.Dark)),

    TypeChart(Type.Ice,
        listOf(Type.Flying, Type.Ground, Type.Grass, Type.Dragon),
        listOf(Type.Steel, Type.Fire, Type.Water, Type.Ice),
        listOf(Type.Ice),
        listOf(Type.Fighting, Type.Rock, Type.Steel, Type.Fire)),

    TypeChart(Type.Dragon,
        listOf(Type.Dragon),
        listOf(Type.Steel, Type.Fairy),
        listOf(Type.Fire, Type.Water, Type.Grass, Type.Electric),
        listOf(Type.Ice, Type.Dragon, Type.Fairy)),

    TypeChart(Type.Dark,
        listOf(Type.Ghost, Type.Psychic),
        listOf(Type.Fighting, Type.Dark, Type.Fairy),
        listOf(Type.Ghost, Type.Psychic, Type.Dark),
        listOf(Type.Fighting, Type.Bug, Type.Fairy)),

    TypeChart(Type.Fairy,
        listOf(Type.Fighting, Type.Dragon, Type.Dark),
        listOf(Type.Poison, Type.Steel, Type.Fire),
        listOf(Type.Fighting, Type.Bug, Type.Dark, Type.Dragon),
        listOf(Type.Poison, Type.Steel))
)
// https://gamepress.gg/pokemongo/pokemon-go-type-chart
// https://www.eurogamer.net/articles/2018-12-21-pokemon-go-type-chart-effectiveness-weaknesses
// https://gamepress.gg/pokemongo/pokemon-go-type-chart
// https://niantic.helpshift.com/a/pokemon-go/?p=web&l=en&s=gyms-battle&f=type-effectiveness-in-battle
