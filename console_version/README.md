This is the command-line (text only) version of **PoGo Types Quiz**.

Prerequisite: [command line Kotlin compiler](https://kotlinlang.org/docs/tutorials/command-line.html).

To compile: 

   make

Or just execute:
  
   kotlinc Pokemon.kt main.kt -include-runtime -d app.jar


To run:

   java -jar app.jar








 
