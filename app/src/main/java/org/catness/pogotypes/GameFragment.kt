/*
 * Copyright 2018, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.catness.pogotypes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import org.catness.pogotypes.databinding.FragmentGameBinding

class GameFragment : Fragment() {
    data class Question(
            val text: String,
            val answers: List<String>,
            val type: Int,
            val typeAnswer: Int,
            val defense: Boolean)

    lateinit var currentQuestion: Question
    lateinit var answers: MutableList<String>
    // the binding class name is derived from layout name
    // fragment_game.xml -> FragmentGameBinding
    private lateinit var binding: FragmentGameBinding 
    var oldColor : Int = 0
    private var questionIndex = 0
    private val numQuestions = 10
    private var numCorrect = 0
    var typeImages : List<Int> = listOf(R.drawable.type_normal,R.drawable.type_fighting,R.drawable.type_flying,R.drawable.type_poison,
                R.drawable.type_ground,R.drawable.type_rock,R.drawable.type_bug,R.drawable.type_ghost,
                R.drawable.type_steel,R.drawable.type_fire,R.drawable.type_water,R.drawable.type_grass,
                R.drawable.type_electric,R.drawable.type_psychic,R.drawable.type_ice,R.drawable.type_dragon,
                R.drawable.type_dark,R.drawable.type_fairy)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate<FragmentGameBinding>(
                inflater, R.layout.fragment_game, container, false)

        // Bind this fragment class to the layout
        binding.game = this
        
        currentQuestion = generateQuestion()
        questionIndex++
        setQuestion(currentQuestion)

        oldColor = (binding.firstAnswerRadioButton.background as ColorDrawable).color
 
        // Set the onClickListener for the submitButton
        binding.submitButton.setOnClickListener @Suppress("UNUSED_ANONYMOUS_PARAMETER")
        { view: View ->
            val checkedId = binding.questionRadioGroup.checkedRadioButtonId
            // Do nothing if nothing is checked (id == -1)
            if (-1 != checkedId) {
                var answerIndex = 0
                when (checkedId) {
                    R.id.secondAnswerRadioButton -> answerIndex = 1
                    R.id.thirdAnswerRadioButton -> answerIndex = 2
                    R.id.fourthAnswerRadioButton -> answerIndex = 3
                }
                // The first answer in the original question is always the correct one, so if our
                // answer matches, we have the correct answer.
                if (answers[answerIndex] == currentQuestion.answers[0]) {
                    numCorrect++
                } else { 
                    // wrong answer
                    when(checkedId) {
                        R.id.firstAnswerRadioButton -> binding.firstAnswerRadioButton.setBackgroundColor(Color.RED)
                        R.id.secondAnswerRadioButton -> binding.secondAnswerRadioButton.setBackgroundColor(Color.RED)
                        R.id.thirdAnswerRadioButton -> binding.thirdAnswerRadioButton.setBackgroundColor(Color.RED)
                        R.id.fourthAnswerRadioButton -> binding.fourthAnswerRadioButton.setBackgroundColor(Color.RED)
                    }
                }

                for (i in 0..3) {
                    if (answers[i] == currentQuestion.answers[0]) {
                        when (i) {
                            0 -> binding.firstAnswerRadioButton.setBackgroundColor(Color.GREEN)
                            1 -> binding.secondAnswerRadioButton.setBackgroundColor(Color.GREEN)
                            2 -> binding.thirdAnswerRadioButton.setBackgroundColor(Color.GREEN)
                            3 -> binding.fourthAnswerRadioButton.setBackgroundColor(Color.GREEN)
                        }
                        break
                    }
                }
                binding.questionMark.setImageResource(typeImages[currentQuestion.typeAnswer])
                binding.nextButton.visibility = View.VISIBLE
                binding.submitButton.visibility = View.GONE

            }
        } // end submitButton

        binding.nextButton.setOnClickListener @Suppress("UNUSED_ANONYMOUS_PARAMETER")
        { view: View ->       
           // Advance to the next question
            if (questionIndex < numQuestions) {
                currentQuestion = generateQuestion()
                questionIndex++
                setQuestion(currentQuestion)
                binding.nextButton.visibility = View.GONE
                binding.submitButton.visibility = View.VISIBLE
                binding.invalidateAll()
            } else {
                // We've won!  Navigate to the gameWonFragment.
            //    view.findNavController()
            //            .navigate(R.id.action_gameFragment_to_gameWonFragment)

                // new version with the safe args plugin:
                // current class name + "Directions"
                // e.g. GameFragment -> GameFragmentDirections
                // and the 2nd part is the same R.id. but in camelCase
                // and with parentheses() like a function call
                // e.g. R.id.action_gameFragment_to_gameWonFragment ->
                // actionGameFragmentToGameWonFragment()
                view.findNavController()
                    .navigate(GameFragmentDirections.actionGameFragmentToGameWonFragment(numQuestions, numCorrect))
                // currently the number of correct questions equals to the total number of questions
                // because we only can share the result if all the questions are answered correctly
            }            
        }

        return binding.root
    }


    // Sets the question and randomizes the answers.  This only changes the data, not the UI.
    // Calling invalidateAll on the FragmentGameBinding updates the data.
    private fun setQuestion(currentQuestion : Question) {
        // randomize the answers into a copy of the array
        answers = currentQuestion.answers.toMutableList()
        // and shuffle them
        answers.shuffle()
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.title_android_trivia_question, questionIndex, numQuestions)

        binding.apply {
            firstAnswerRadioButton.setBackgroundColor(oldColor)
            secondAnswerRadioButton.setBackgroundColor(oldColor)         
            thirdAnswerRadioButton.setBackgroundColor(oldColor)         
            fourthAnswerRadioButton.setBackgroundColor(oldColor)           
            questionImage.setImageResource(typeImages[currentQuestion.type])
            questionMark.setImageResource(R.drawable.question_mark)
            arrowImage.setScaleX( if (currentQuestion.defense) -1f else 1f )
        }
    }

    private fun generateQuestion() : Question {
        var size: Int = types.size-1
        var numType = (0..size).random()   // type we choose in the question
        var quality = (0..3).random()
        
        val correctList : List<Type> =                                                                                                                             
            when(quality) {                                                                                                                                        
                0 -> types[numType].strong                                                                                                                         
                1 -> types[numType].weak
                2 -> types[numType].resist
                3 -> types[numType].vulnerable
                else -> listOf(Type.None) // shouldn't happen
            }
       
        // if the selected quality is relevant to attack or defense
        val defense : Boolean = (quality >=2)

        var sizeCorrect : Int = correctList.size-1
        var correct = (0..sizeCorrect).random()   // one of the correct answers : number
        val correctAnswer = correctList[correct]  // the value of the correct answer
        val incorrectList : List<Type> = allTypes.minus(correctList)
        val shuffled = incorrectList.shuffled()
        var answers : List<String> = listOf(
            correctAnswer.name,
            shuffled[0].name,
            shuffled[1].name,
            shuffled[2].name
        )
        val text = "${types[numType].type} ${qualities[quality]}:"
        return Question(text,answers,numType,correctAnswer.ordinal,defense)
    }


}
